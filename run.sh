#!/bin/sh

user=`whoami`
if [ -z $user ]; then
    echo "Something is wrong with the LDAP/NSS"
    exit 1
fi

if [ "$user" = "root" ]; then
    echo "Running this command as buildbot user"
    su -c ./run.sh buildbot
    exit 0
fi

if [ "$user" != "buildbot" ]; then
    echo "This command must be run as root"
    exit 1
fi

env_dir=`pwd`
fluendo_buildbot_dir=$env_dir/fluendo-buildbot
master_dir=$fluendo_buildbot_dir/buildbot/master

# Override the python path with our own path
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export PYTHONPATH=$fluendo_buildbot_dir:$master_dir:${PYTHONPATH}

if [ -z "$BUILDMASTER_BASEDIR" ]
then
  export BUILDMASTER_BASEDIR=$fluendo_buildbot_dir
else
  # create the BUILDMASTER_BASEDIR if necessary
  if [ ! -d "$BUILDMASTER_BASEDIR" ]
  then
    mkdir -p $BUILDMASTER_BASEDIR
    chown builbot $BUILDMASTER_BASEDIR
  fi
fi

# Fetch the buildbot project
git clone git@github.com:fluendo/fluendo-buildbot.git
cd fluendo-buildbot
if [ "$FLUENDO_BUILDBOT_REPO_BRANCHx" != "x" ]
then
  git checkout origin/$FLUENDO_BUILDBOT_REPO_BRANCH
fi
git submodule init
git submodule update

# copy the master.cfg and buildbot.tac to BUILDMASTER_BASEDIR
if [ -f $fluendo_buildbot_dir/master.cfg ]
then
  cp $fluendo_buildbot_dir/master.cfg $BUILDMASTER_BASEDIR
else
  echo "$fluendo_buildbot_dir/master.cfg can not be found. Exit"
  exit 1
fi
if [ -f $fluendo_buildbot_dir/buildbot.tac ]
then
  cp $fluendo_buildbot_dir/buildbot.tac $BUILDMASTER_BASEDIR
else
  echo "$fluendo_buildbot_dir/master.cfg can not be found. Exit"
  exit 1
fi

# Link our own change_hook
ln -s $fluendo_buildbot_dir/flubot/flubuild.py $master_dir/buildbot/www/hooks

# wait for db to start by trying to upgrade the master
until buildbot upgrade-master $BUILDMASTER_BASEDIR
do
  echo "Can't upgrade master yet. Waiting for database ready?"
  sleep 1
done

# we use exec so that twistd use the pid 1 of the container, and so that signals are properly forwarded
exec twistd -ny $BUILDMASTER_BASEDIR/buildbot.tac
