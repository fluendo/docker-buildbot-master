FROM ubuntu:16.04

MAINTAINER fluendo

ENV DEBIAN_FRONTEND noninteractive

# Create the worker dir
RUN mkdir /master
RUN chmod 777 /master
WORKDIR /master

# Install required packages
RUN apt-get update && \
    apt-get upgrade -y && \ 
    apt-get install -y \
        python2.7 \
        python2.7-dev \
        python-setuptools \
        python-pip \
        libffi-dev \
        libssl-dev \
        git

RUN pip install buildbot[bundle] \
        pyOpenSSL \
        certifi \
        twisted==16.1.0 \
        txrequests \
        requests \
        tinys3 \
        docker

# Install ldap support
RUN apt-get -y install sudo wget libnss-ldap

EXPOSE 8076
EXPOSE 9076
COPY run.sh .
CMD ["./run.sh"]
